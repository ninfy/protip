---
layout: default
---

## Mở đầu:

Bài dịch kiểm tra đầu vào của STS thường khó hơn 80% các văn bản khi mọi người đã dịch thực tế.

Một người được quyền nộp tham dự 3 lần. Nếu trượt cả 3 sẽ bị tạm dừng cho gửi bài tham dự trong khoảng vài ngày đến vài tuần. 

### Các thứ không dịch -  vi phạm loại thẳng.
* Tên trò chơi
* Tên DLC
* Tên nhà phát hành
* HTML code
* Không dịch số thành chữ, và ngược lại

### Các thứ phải dịch (và thường hay quên dịch):
* Từ "Game"
* Các thuật ngữ của trò chơi: Như Class, 
* Thể loại (Genre), cố dịch xúc tích, ngắn gọn. Ví dụ:
    * RPG = Nhập vai
    * Turn based strategy = Chiến thuật theo lượt

### Các từ ngữ tiền lệ: 
Có các thuật ngữ, danh từ riêng đã mang tính văn hóa cao, từng dịch trước đó, mang tính phổ thông và đại trà cao v.v… thì người dịch phải thể hiện sự đầu tư bài dịch bằng cách áp dụng cách dịch đúng và chuẩn. 
Ví dụ:
* Gone with the wind = Cuốn theo chiều gió
* Vanity Fair = Hội chợ phù hoa
* Dumb Luck = Số đỏ (Tác phẩm của Vũ Trọng Phụng)
* Mad Hatter = Người bán nón điên
* Cheshire Cat = Mèo Cheshire

## Nghiên cứu ngữ cảnh

Một bài dịch tốt tức nên có sự đầu tư về ngữ cảnh để hiểu xem từ ngữ mô tả cái gì. Rất nhiều câu dịch khó có thể hoàn toàn giải quyết nếu thấy hình ảnh hay video liên quan. 
Hãy dùng Google
* Tra đoạn văn: Để biết xem mình đang dịch đoạn văn của trò chơi gì
* Tra từ khóa trên wiki của trò chơi: Để biết xem nên dịch diễn đạt lại vật phẩm, tên chế độ trò chơi v.v…

## Gợi ý khác
* Kiểm tra lại lỗi dấu chấm, dấu phẩy, khoảng cách trước khi nộp chính thức.
* Bản dịch tiếng Việt hãy quăng cho một đứa bạn, xem nó đọc có thấy hiểu được tiếng Việt mình vừa dịch không.
* Cải thiện văn phong, tránh lặp từ nếu có thể.
* Đừng ngại nhắn tin trên [Discord chính thức của STS-VI](http://discord.io/stsvi) để được giúp đỡ từ các moderator và các tình nguyện viên ở STS Việt Nam.

## Điểm cộng

Các quy tắc chính thức của STS Việt Nam, không phải là lỗi trừ điểm. Nhưng nếu theo được thì là điểm cộng.

<table>
  <tr>
    <td>Format ngày tháng năm</td>
    <td>Tuesday, September 12th, 2017</td>
    <td>Thứ ba, 12 tháng 9, 2017


</td>
    <td>(Tuyệt đối không ghi thành Thứ 3)</td>
  </tr>
  <tr>
    <td>Time zone</td>
    <td>September 12th, 7 pm EST,</td>
    <td>Ngày 13 tháng 9, 7:00 giờ Việt Nam,</td>
    <td>Chuyển về format 24h, theo giờ Việt Nam GMT +7

http://www.thetimezoneconverter.com/
</td>
  </tr>
  <tr>
    <td>Danh từ in hoa (Steam)</td>
    <td>This is Steam Subscriber Agreement, you must AGREE on the continue. </td>
    <td>Đây là điều khoản người đăng ký Steam, bạn phải ĐỒNG Ý để tiếp tục.</td>
    <td>Trừ phi câu cú gây dễ lẫn lộn, thì mới được phép viết thành "Điều khoản người đăng ký Steam".
Đây là quy tắc, miễn thảo luận lại.
Steam gần như luôn viết hoa trong mọi ngữ cảnh.</td>
  </tr>
  <tr>
    <td>Danh từ in hoa (Các phân khu còn lại)</td>
    <td></td>
    <td></td>
    <td>Tùy theo yêu cầu, người phụ trách của từng phân khu (Dota, CSGO, Half Life v.v…)</td>
  </tr>
  <tr>
    <td>Trật tự cụm danh từ viết hóa</td>
    <td>This is exclusive reward for The International 2017 Battle Pass owner</td>
    <td>Phần thưởng này dành riêng cho người sở hữu Battle Pass The International 2017</td>
    <td>Trong ví dụ này, Battle Pass là danh từ chủ, sẽ đem lên trước.</td>
  </tr>
  <tr>
    <td>Tiền tệ</td>
    <td>Total reward is $1,600,000 (Equal to [your currency here])</td>
    <td>Tổng giải thưởng là $1,600,000 (Tương đương 36,36 tỷ Việt Nam đồng)</td>
    <td>Theo quy tắc ngân hàng, giữ trật tự quy tắc phân cách của tiền tệ gốc. 
Quy tắc tiền tệ Việt Nam: 
100.000,500 = Một trăm ngàn năm trăm đồng</td>
  </tr>
  <tr>
    <td>Số, phân cách thập phân.</td>
    <td>1,000,000 Steam users and growing</td>
    <td>1.000.000 người dùng Steam và ngày một tăng lên</td>
    <td>Chuyển thể về quy tắc phân cách của Việt Nam. Với các số nhỏ tầm ngàn thì có thể bỏ qua. </td>
  </tr>
  <tr>
    <td>…</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
</table>


## Mẹo văn phong

<table>
  <tr>
    <td>Original</td>
    <td>Average Example</td>
    <td>Better Example</td>
    <td>Why</td>
  </tr>
  <tr>
    <td>The western forests guard their secrets well</td>
    <td>Những cánh rừng phía tây biết cách giữ những bí mật của chúng.</td>
    <td>Những cánh rừng mạn tây biết cách giữ bí mật của chúng. </td>
    <td>Từ mạn tây là đồng nghĩa, đồng thời tăng độ phong phú từ ngữ. Đôi khi dùng từ quá phổ biến làm câu văn nhàm chán.
Giảm bớt 1 từ những, tránh lặp từ. Giảm số nhiều ở những nơi không cần thiết</td>
  </tr>
  <tr>
    <td>One of these is Lyralei, master archer of the wood, and favored godchild of the wind. 
</td>
    <td>Một trong những bí mật đó là Lyralei, cung thủ bậc thầy của rừng, đứa con nuôi của gió.</td>
    <td>Một trong các bí mật đó là Lyralei, cung thủ bậc thầy của rừng xanh, đứa con nuôi yêu quý của thần gió. 
</td>
    <td>Các: Thay vì dùng từ những, vốn đã có ở câu trên. Đa dạng từ ngữ.
Rừng xanh: Văn bản gốc không có từ xanh, có thể biến hóa để cho câu văn chau chuốt. Một từ được dùng do ảnh hưởng của Robin Hood of Sherwood -> Robin Hood của rừng xanh v.v…
Thần gió: Hiểu được ngữ cảnh để dịch được vế The wind thành thần gió. 
</td>
  </tr>
  <tr>
    <td>The wind pitied the child and so lifted her into the sky and deposited her on a doorstep in a neighboring village.
</td>
    <td>Gió cảm thấy tội nghiệp và mang đứa trẻ lên tận bầu trời và gửi nó lại vào thềm cửa của một ngôi làng bên cạnh. </td>
    <td>Gió cảm thấy tội nghiệp mà mang đứa trẻ lên tận bầu trời, gửi nó lại vào thềm cửa của một ngôi làng bên cạnh. </td>
    <td>Văn bản gốc có 2 từ "and", lặp không cần thiết. Nếu dịch sát, chúng ta sẽ có tới 2 từ “và”.
Thay vào đó, bản dịch tốt dùng từ “mà” (Do ngữ cảnh cho phép thế), và dấu phảy “,” thay cho từ “và”. Tuy thế, có thể dùng từ “và” do lần trước đã dùng từ “mà”.</td>
  </tr>
  <tr>
    <td>In the years that followed, the wind returned occasionally to the child's life, watching from a distance while she honed her skills. 
</td>
    <td>Những năm tiếp theo, gió thi thoảng quay lại để ngắm đứa con mình, từ xa trông ngóng cô con gái trui rèn kỹ năng của mình.</td>
    <td>Những tháng ngày sau đó, gió thi thoảng quay lại để ngắm đứa con mình, từ xa trông ngóng cô con gái trui rèn kỹ năng của mình. </td>
    <td>“In the years that followed” – câu này là một cách biểu đạt thời gian trôi qua. Với những câu mang tính biểu đạt, có thể vận dụng thoải mái và dịch thoát nghĩa hơn. Ở đây dịch thành: Những tháng ngày sau đó -> Hoàn toàn không có từ năm (year), nhưng vẫn diễn đạt được ý câu.</td>
  </tr>
</table>
